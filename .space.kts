/**
* JetBrains Space Automation
* This Kotlin-script file lets you automate build activities
* For more info, see https://www.jetbrains.com/help/space/automation.html
*/

// STAGING
job("Build, push and deploy staging") {
    startOn {
        gitPush {
            branchFilter {
                +Regex("staging")
            }
        }
    }

    container(displayName = "Build and push", image = "docker:dind") {
        shellScript {
            content = """
            	apk update
                apk add git
                git submodule update --init
                docker login -u a84baedd-a48f-41b6-845e-2c54dc21b6fa -p 25ecc99f3d89402db4abf545a7ac1ed48b48dab093a40f96dd1e656b4cd66399 planblick.registry.jetbrains.space
                docker build -t planblick.registry.jetbrains.space/p/planblick/images/pytestkube:staging -t planblick.registry.jetbrains.space/p/planblick/images/pytestkube:${'$'}JB_SPACE_GIT_REVISION .
                docker push planblick.registry.jetbrains.space/p/planblick/images/pytestkube:staging
                docker push planblick.registry.jetbrains.space/p/planblick/images/pytestkube:${'$'}JB_SPACE_GIT_REVISION
            """
        }
    }
}

// PRODUCTION
job("Build, push and deploy production") {
    startOn {
        gitPush {
            branchFilter {
                +Regex("production")
            }
        }
    }

    container(displayName = "Build and push", image = "docker:dind") {
        shellScript {
            content = """
            	apk update
                apk add git
                git submodule update --init
                docker login -u a84baedd-a48f-41b6-845e-2c54dc21b6fa -p 25ecc99f3d89402db4abf545a7ac1ed48b48dab093a40f96dd1e656b4cd66399 planblick.registry.jetbrains.space
                docker build -t planblick.registry.jetbrains.space/p/planblick/images/pytestkube:staging -t planblick.registry.jetbrains.space/p/planblick/images/pytestkube:${'$'}JB_SPACE_GIT_REVISION .
                docker push planblick.registry.jetbrains.space/p/planblick/images/pytestkube:production
                docker push planblick.registry.jetbrains.space/p/planblick/images/pytestkube:${'$'}JB_SPACE_GIT_REVISION
            """
        }
    }
}
