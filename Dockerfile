FROM python:alpine

WORKDIR /tmp
RUN apk add --update --no-cache curl openssl gcc libc-dev linux-headers jpeg-dev openjpeg-dev zlib-dev libffi-dev git

RUN pip install pytest pytest-bdd pytest-html pytest-xdist pytest-xdist[psutil] pytest-splinter pillow

RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl

CMD ["sh"]
